$('#aparencia').click(function(){
  shell.exec('xfce4-appearance-settings',(a,b,c)=>{
  });
});

$('#gerJanelas').click(function(){
  shell.exec('xfwm4-settings',(a,b,c)=>{
  });
});

$('#ajustesJanelas').click(function(){
  shell.exec('xfwm4-tweaks-settings',(a,b,c)=>{
  });
});

$('#appPreferido').click(function(){
  shell.exec('exo-preferred-applications',(a,b,c)=>{
  });
});

$('#areaTrabalho').click(function(){
  shell.exec('xfdesktop-settings',(a,b,c)=>{
  });
});

$('#espacosTrabalho').click(function(){
  shell.exec('xfwm4-workspace-settings',(a,b,c)=>{
  });
});

$('#notify').click(function(){
  shell.exec('xfce4-notifyd-config',(a,b,c)=>{
  });
});

$('#painel').click(function(){
  shell.exec('xfce4-panel --preferences',(a,b,c)=>{
  });
});

$('#mudarInterface').click(function(){
  shell.exec('xfpanel-switch',(a,b,c)=>{
  });
});

$('#transpJanelas').click(function(){
  shell.exec('/usr/bin/xfwm4-composite-editor',(a,b,c)=>{
  });
});
////////////////////////////////////////////////////////////////
$('#energy').click(function(){
  shell.exec('xfce4-power-manager-settings',(a,b,c)=>{
  });
});

$('#midiasRemoviveis').click(function(){
  shell.exec('thunar-volman-settings',(a,b,c)=>{
  });
});

$('#monitor').click(function(){
  shell.exec('xfce4-display-settings',(a,b,c)=>{
  });
});

$('#mouse').click(function(){
  shell.exec('xfce4-mouse-settings',(a,b,c)=>{
  });
});

$('#teclado').click(function(){
  shell.exec('xfce4-keyboard-settings',(a,b,c)=>{
  });
});

$('#dataHora').click(function(){
  shell.exec('gksu time-admin',(a,b,c)=>{
  });
});

$('#globalDataHora').click(function(){
  shell.exec('orage -p',(a,b,c)=>{
  });
});


$('#print').click(function(){
  shell.exec('system-config-printer',(a,b,c)=>{
  });
});

$('#montDiscos').click(function(){
  shell.exec('gnome-disk-image-mounter',(a,b,c)=>{
  });
});

$('#disks').click(function(){
  shell.exec('gnome-disks',(a,b,c)=>{
  });
});
////////////////////////////////////////////////////////////////
$('#stacer').click(function(){
  shell.exec(' stacer',(a,b,c)=>{
  });
});

$('#dki').click(function(){
  shell.exec('/opt/dki/dki',(a,b,c)=>{
  });
});

$('#access').click(function(){
  shell.exec('xfce4-accessibility-settings',(a,b,c)=>{
  });
});

$('#editMIME').click(function(){
  shell.exec('xfce4-mime-settings',(a,b,c)=>{
  });
});

$('#servicos').click(function(){
  shell.exec('services-admin',(a,b,c)=>{
  });
});

$('#update').click(function(){
  shell.exec('mintupdate',(a,b,c)=>{
  });
});

$('#inicializacao').click(function(){
  shell.exec('xfce4-session-settings',(a,b,c)=>{
  });
});

$('#usuario').click(function(){
  shell.exec('/usr/bin/mugshot',(a,b,c)=>{
  });
});

$('#usuarios').click(function(){
  shell.exec('users-admin',(a,b,c)=>{
  });
});

///////////////////////////////////////////
$('#software').click(function(){
  shell.exec('gnome-software',(a,b,c)=>{
  });
});

$('#packdeb').click(function(){
  shell.exec('deepin-deb-installer',(a,b,c)=>{
  });
});



///////////////////////////////////////////
$('#netAdmin').click(function(){
  shell.exec('network-admin',(a,b,c)=>{
  });
});

$('#conecRede').click(function(){
  shell.exec('nm-connection-editor',(a,b,c)=>{
  });
});

///////////////////////////////////////////
$('#editConfig').click(function(){
  shell.exec('xfce4-settings-editor',(a,b,c)=>{
  });
});
