const electron = require('electron');
const {app, BrowserWindow} = electron;

app.on('ready', () => {
    let win = new BrowserWindow({
        width: 800,
        height: 600,
        show: false,
        icon: __dirname + '/img/SettingDZ.png'
        
//        resizable: false
    })


    win.loadURL('file://' + __dirname + '/index.html');

    win.on('ready-to-show', () => {
        win.show()
    })


//    win.webContents.openDevTools();
    
    win.on('closed', () => win = null)

});

app.on('window-all-closed', () => {
    app.quit()
})


